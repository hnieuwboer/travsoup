package org.travsoup.data;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 16-5-13
 * Time: 18:13
 */
public enum Page {
    OUTER_VILLAGE("dorf1.php"),
    INNER_VILLAGE("dorf2.php");
    private final String url;
    private Page(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
