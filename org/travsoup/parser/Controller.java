package org.travsoup.parser;

import org.jsoup.nodes.Document;
import org.travsoup.Settings;
import org.travsoup.data.Page;
import org.travsoup.wrappers.Session;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 16-5-13
 * Time: 19:33
 */
public class Controller {

    private static final File HANDLER_FOLDER = new File(".".concat(Settings.HANDLER_PACKAGE.replaceAll("\\.", "/")));
    private final Session session;
    private final List<PageHandler> handlerList = new ArrayList<>();
    private final EnumMap<Page, Document> cachedDocuments;
    private final EnumMap<Page, Long> updateMap;
    public Controller(Session session) {
        this.session = session;
        this.cachedDocuments = new EnumMap<>(Page.class);
        this.updateMap = new EnumMap<>(Page.class);
        loadHandlers();
    }

    private void loadHandlers() {
        if(!HANDLER_FOLDER.exists()) {
            throw new RuntimeException("Error loading handlers");
        }
        try {
            URLClassLoader loader = URLClassLoader.newInstance(new URL[]{ HANDLER_FOLDER.toURI().toURL() });
            for(File file : HANDLER_FOLDER.listFiles()) {
                String s = file.getName();
                if(s.endsWith(".class")) {
                    try {
                        Class<?> clazz = loader.loadClass(Settings.HANDLER_PACKAGE.concat(s.substring(0, s.length() - 6)));
                        Class<? extends PageHandler> handlerClazz = clazz.asSubclass(PageHandler.class);
                        Constructor<? extends PageHandler> handler = handlerClazz.getDeclaredConstructor(Controller.class);
                        handlerList.add(handler.newInstance(this));
                    } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Session getSession() {
        return session;
    }

    public boolean update() {
        for(Page page : Page.values()) {
            try {
                Document d = getDocument(page);
                for (PageHandler pageHandler : handlerList) {
                    if (pageHandler.getClass().isAnnotationPresent(Handler.class)) {
                        Handler handler = pageHandler.getClass().getAnnotation(Handler.class);
                        if (page == handler.page()) {
                            pageHandler.handle(d);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private Document getDocument(Page page) throws IOException {
        if(System.currentTimeMillis() - updateMap.get(page) > 60000) {
            Document doc = session.getConnection(page.getUrl()).get();
            updateMap.put(page, System.currentTimeMillis());
            cachedDocuments.put(page, doc);
            return doc;
        } else {
            return cachedDocuments.get(page);
        }
    }

}
