package org.travsoup.parser;

import org.travsoup.data.Page;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 16-5-13
 * Time: 18:16
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Handler {
    String name();
    Page page();
}
