package org.travsoup.parser;

import org.jsoup.nodes.Document;
import org.travsoup.wrappers.Session;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 16-5-13
 * Time: 18:15
 */
public abstract class PageHandler {

    protected final Controller controller;
    public PageHandler(Controller controller) {
        this.controller = controller;
    }

    protected final Future<?> submit(Runnable runnable) {
        return submit(runnable, 0);
    }

    protected final Future<?> submit(Runnable runnable, long seconds) {
        Session session = controller.getSession();
        ScheduledExecutorService executor = session.getExecutor();
        return executor.schedule(runnable, seconds, TimeUnit.SECONDS);
    }
    public abstract void handle(Document document);
}
