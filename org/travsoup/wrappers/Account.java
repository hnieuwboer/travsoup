package org.travsoup.wrappers;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 12-5-13
 * Time: 16:14
 */
public class Account {

    private final String server;
    private final String account;
    private final String password;
    public Account(String server, String account, String password) {
        this.server = server;
        this.account = account;
        this.password = password;
    }

    public Session login() {
        try {
            Connection c = Jsoup.connect(server + "dorf1.php")
                    .data("name", account)
                    .data("password", password)
                    .data("w", "1280:800")
                    .data("s1", "Login")
                    .data("lowRes", "0")
                    .data("login", String.valueOf(System.currentTimeMillis()));
            Document d = c.post();
            return new Session(this, c.response().cookies());
        } catch (Exception e) {
            return null;
        }
    }

    public String getServer() {
        return server;
    }

    public String getAccount() {
        return account;
    }

}
