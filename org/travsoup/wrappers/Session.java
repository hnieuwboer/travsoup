package org.travsoup.wrappers;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 12-5-13
 * Time: 16:21
 */
public class Session {

    private static final Pattern X_Y_PATTERN = Pattern.compile("\\((-?\\d*)|(-?\\d*)\\)");
    private final Account account;
    private final Map<String, String> cookies;
    private final List<Village> villages;
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private Village current;
    public Session(Account account, Map<String, String> cookies) {
        this.account = account;
        this.cookies = cookies;
        this.villages = new ArrayList<>();
        indexVillages();
    }

    private void indexVillages() {
        try {
            Document d = getConnection("dorf1.php").get();
            for(Element e : d.select("a[href*=newdid]")) {
                String name = e.text();
                int id = Integer.parseInt(e.attr("href").replaceAll("\\D", ""));
                Matcher m = X_Y_PATTERN.matcher(e.attr("title"));
                int x = m.find() ? Integer.parseInt(m.group(1)) : 0;
                int y = m.find() ? Integer.parseInt(m.group(2)) : 0;
                Village v = new Village(name, id, x, y);
                villages.add(v);
                if(e.attr("class").equals("active")) {
                    current = v;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ScheduledExecutorService getExecutor() {
        return executor;
    }

    public Account getAccount() {
        return account;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public Village getCurrent() {
        return current;
    }

    public Connection getConnection(String page) {
        return Jsoup.connect(account.getServer() + page).cookies(cookies);
    }
}
