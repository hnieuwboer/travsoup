package org.travsoup.wrappers;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 17-5-13
 * Time: 21:29
 */
public class Village {

    private final String name;
    private final int id;
    private final int x;
    private final int y;
    public Village(String name, int id, int x, int y) {
        this.name = name;
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Village: ");
        sb.append(name);
        sb.append(" ID:");
        sb.append(id);
        sb.append(" AT (");
        sb.append(x);
        sb.append(",");
        sb.append(y);
        sb.append(")");
        return sb.toString();
    }

}
